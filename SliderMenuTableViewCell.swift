//
//  SliderMenuTableViewCell.swift
//  Clickat
//
//  Created by Sierra 4 on 10/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class SliderMenuTableViewCell: UITableViewCell {
    
    @IBOutlet var lblHome: UILabel!
    @IBOutlet var imageHome: UIImageView!
    var object: TableClass?{
        didSet{
            updateUI()
        }
    }
    fileprivate func updateUI(){
        lblHome?.text = object?.name
        imageHome?.image = object?.image
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
