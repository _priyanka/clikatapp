//
//  Disappear.swift
//  Clickat
//
//  Created by Sierra 4 on 10/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import  UIKit

class Disappear
{
    class func backSlideLeft(obj:UIViewController)
    {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            var frameMenu : CGRect = obj.view.frame
            frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
            obj.view.frame = frameMenu
            obj.view.layoutIfNeeded()
            obj.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            obj.view.removeFromSuperview()
        })
}
}
