

import UIKit
import ObjectMapper
import SVProgressHUD

class ViewController: UIViewController {
    @IBOutlet var buttonOutlet: UIButton!
    var headerView : ViewSectionHeader?
    @IBOutlet var TableViewOutlet: UITableView!
    var userModel : Instructions?
    override func viewDidLoad() {
        super.viewDidLoad()
        let swipeRight = UISwipeGestureRecognizer(target: self, action:#selector(hole))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action:#selector(hole))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        TableViewOutlet.estimatedRowHeight = 200
        TableViewOutlet.rowHeight = UITableViewAutomaticDimension
        
        
        let nibOne = UINib(nibName: TableCellNibNames.advertise.identifiersOne, bundle: nil)
        TableViewOutlet.register(nibOne, forCellReuseIdentifier: Products.advertise.identifiersTwo )
        let nibTwo = UINib(nibName: TableCellNibNames.categories.identifiersOne, bundle: nil)
        TableViewOutlet.register(nibTwo, forCellReuseIdentifier: Products.categories.identifiersTwo )
        let nibThree = UINib(nibName: TableCellNibNames.offers.identifiersOne, bundle: nil)
        TableViewOutlet.register(nibThree, forCellReuseIdentifier: Products.offers.identifiersTwo)
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)

        self.fetchData()
    }
    func fetchData()
    {
        let param:[String:Any] = [JsonData.areaId.idJson:JsonData.areaIdString.idJson,JsonData.countryId.idJson:JsonData.countryIdString.idJson]
        Api.fetchData(parameters: param) { (jsonData) in
        print(jsonData!)
        self.userModel = Mapper<Instructions>().map(JSONObject: jsonData)
        self.TableViewOutlet.reloadData()
        print(self.userModel?.message ?? Unwrapping.nilWrap.idNil)
        print(jsonData!)
        SVProgressHUD.dismiss()
         }
    }
    @IBAction func buttonAction(_ sender: UIButton) {
        Appear.appearSlideLeft(obj: self){_ in
        }
    }
        func hole(gesture: UISwipeGestureRecognizer)
        {
            let swipeGesture = gesture as UISwipeGestureRecognizer
                if (swipeGesture.direction == UISwipeGestureRecognizerDirection.right)
                {
                    Appear.appearSlideLeft(obj: self){_ in
                    }
                }
            }
    }
extension ViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
       return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section)
        {
        case 0:
            let cellOne = tableView.dequeueReusableCell(withIdentifier: Products.advertise.identifiersTwo , for: indexPath) as! TableViewCell
            return cellOne
        case 1:
            let cellTwo = tableView.dequeueReusableCell(withIdentifier: Products.categories.identifiersTwo , for: indexPath) as! TableViewCellTwo
            cellTwo.categories = self.userModel?.data?.english
            cellTwo.collectionOutletTwo.reloadData()
            return cellTwo
        case 2,3:
            let cellThree = tableView.dequeueReusableCell(withIdentifier: Products.offers.identifiersTwo , for: indexPath) as! TableViewCellThird
            cellThree.sectionCall = indexPath.section
            return cellThree
        default:
            let cellTwo = tableView.dequeueReusableCell(withIdentifier: Products.categories.identifiersTwo, for: indexPath) as!
            TableViewCellTwo
            return cellTwo
    }
}
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section
        {
        case 1:
            return (6*155)+40
        default :
            return 200
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = UINib(nibName: ViewNibName.nibOne.idview, bundle: nil)
        let headerView = nib.instantiate(withOwner: nil, options: nil)[0] as? ViewSectionHeader
        if(section == 2){
       // headerView?.viewAllOutlet.textColor = UIColor.white
        headerView?.uiviewOutlet.backgroundColor = UIColor(red: 218/255, green: 232/255, blue: 230/255, alpha: 1)
        headerView?.viewAllOutlet.font = UIFont(name: (headerView?.viewAllOutlet.font.fontName)!, size: 13)
        headerView?.viewAllOutlet.text = ViewString.offer.idOfferRec
        headerView?.buttonOutlet.titleLabel!.font =  UIFont(name: Font.font.idFont, size: 13)
        headerView?.buttonOutlet.isHidden = false
        return headerView
        }else{
            headerView?.viewAllOutlet.font = UIFont(name:(headerView?.viewAllOutlet.font.fontName)!, size: 13)
             headerView?.uiviewOutlet.backgroundColor = UIColor(red: 218/255, green: 232/255, blue: 230/255, alpha: 1)
            headerView?.viewAllOutlet.text = ViewString.recommonded.idOfferRec
            headerView?.buttonOutlet.isHidden = true
            return headerView
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 2 || section == 3)
        {
            return 30
        }else{
            return 0
        }
    }

}
