//
//  TableViewCell.swift
//  Clickat
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    
    @IBOutlet var viewOutlet: UIView!
    @IBOutlet var collectionViewOutlet: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
         Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(pageSlide), userInfo: nil, repeats: true)
        let nib = UINib(nibName: CollectionCellNibName.advertise.identifiersThree, bundle: nil)
        collectionViewOutlet.register(nib, forCellWithReuseIdentifier: CellProducts.advertise.identifiersFour)
        collectionViewOutlet.dataSource = self
        collectionViewOutlet.delegate = self
        viewOutlet.layer.cornerRadius = 3
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension TableViewCell : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellProducts.advertise.identifiersFour, for: indexPath)
        return cell
    }
    func pageSlide(){
        
        let pageWidth:CGFloat = self.collectionViewOutlet.frame.width
        let maxWidth:CGFloat = pageWidth * 4
        let contentOffset:CGFloat = self.collectionViewOutlet.contentOffset.x
        var slideToX = contentOffset + pageWidth
        if  contentOffset + pageWidth == maxWidth
        {
            slideToX = 0
        }
        self.collectionViewOutlet.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.collectionViewOutlet.frame.height), animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let myWidth = self.collectionViewOutlet.frame.width
        let myHeight = self.collectionViewOutlet.frame.height
        return CGSize(width: myWidth, height:myHeight)
    }

}

