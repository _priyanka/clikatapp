//
//  Appear.swift
//  Clickat
//
//  Created by Sierra 4 on 10/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import UIKit
var flag = true
var flagtwo = true
class Appear{
    
    
    class func appearSlideLeft(obj: UIViewController , completionHandler:@escaping() -> ())
    {
        let viewController : SlideViewController = obj.storyboard!.instantiateViewController(withIdentifier: StoryBoard.identifierStory.idStory) as! SlideViewController
        obj.view.addSubview(viewController.view)
        obj.addChildViewController(viewController)
        viewController.view.layoutIfNeeded()
        viewController.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            viewController.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        }, completion:{ completed in
            if completed {
                completionHandler()
            }
        })
}
}

