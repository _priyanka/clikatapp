//
//  TableViewCellThird.swift
//  Clickat
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class TableViewCellThird : UITableViewCell {
    @IBOutlet var collectionViewThird: UICollectionView!
    var sectionCall: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = UINib(nibName: CollectionCellNibName.offersOne.identifiersThree, bundle: nil)
        collectionViewThird.register(nib, forCellWithReuseIdentifier: CellProducts.offersTwo.identifiersFour)
        let nibSecond = UINib(nibName: CollectionCellNibName.recommondedOne.identifiersThree, bundle: nil)
        collectionViewThird.register(nibSecond, forCellWithReuseIdentifier: CellProducts.recommondedTwo.identifiersFour )
        collectionViewThird.dataSource = self
        collectionViewThird.delegate = self
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
}
}
extension TableViewCellThird : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(sectionCall == 2){
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellProducts.offersTwo.identifiersFour, for: indexPath)
       cell.layer.cornerRadius = 1
        return cell
    }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellProducts.recommondedTwo.identifiersFour, for: indexPath)
            cell.layer.cornerRadius = 1
            return cell
        }
}
}
