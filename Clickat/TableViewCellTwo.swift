//
//  TableViewCellTwo.swift
//  Clickat
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import Kingfisher

class TableViewCellTwo: UITableViewCell {
    @IBOutlet var collectionOutletTwo: UICollectionView!
     var categories : [English]?
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = UINib(nibName: CollectionCellNibName.categories.identifiersThree, bundle: nil)
        collectionOutletTwo.register(nib, forCellWithReuseIdentifier: CellProducts.categories.identifiersFour )
        collectionOutletTwo.dataSource = self
        collectionOutletTwo.delegate = self
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
extension TableViewCellTwo : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories?.count ?? 11
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellProduct = collectionView.dequeueReusableCell(withReuseIdentifier: CellProducts.categories.identifiersFour, for: indexPath) as! CollectionViewCellTwo
      cellProduct.layer.cornerRadius = 5
      cellProduct.layer.masksToBounds = true
      cellProduct.lblCatOutlet.text = categories?[indexPath.row].name
      print(categories?[indexPath.row].name ?? Unwrapping.nilWrap.idNil)
      let url = URL(string: categories?[indexPath.row].image ?? Unwrapping.nilWrap.idNil)
      cellProduct.imageOutlet.kf.setImage(with: url)
      cellProduct.imageOutlet.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "images 6"))
      return cellProduct
}
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               if(indexPath.item==10){
                   return CGSize(width: 285, height: 150)
               }
               else{
                    return CGSize(width: 139, height: 150)
        }
    }
}

