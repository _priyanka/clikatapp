//
//  Enum.swift
//  Clickat
//
//  Created by Sierra 4 on 15/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import UIKit

enum TableCellNibNames : String{
    case advertise = "TableViewCell"
    case categories = "TableViewCellTwo"
    case offers = "TableViewCellThird"
    var identifiersOne : String{
        return self.rawValue
    }
}
enum Products : String{
    case advertise = "TableCellOne"
    case categories = "TableTwoIdentifier"
    case offers = "TableThreeIdentifier"
    var identifiersTwo  : String{
        return self.rawValue
    }
}
enum CollectionCellNibName : String{
    case advertise = "CollectionViewCell"
    case categories = "CollectionViewCellTwo"
    case offersOne = "CollectionViewCellThird"
    case recommondedOne = "CollectionViewCellFourth"
    var identifiersThree : String{
        return self.rawValue
    }
}
enum CellProducts : String{
    case advertise = "CollectionIdentifier"
    case categories = "IdentifierCategory"
    case offersTwo = "IdentifierOffers"
    case recommondedTwo = "IdentifierRecommended"
    var identifiersFour : String{
        return self.rawValue
    }
}
enum CellNames : String{
    case home = "Home"
    case live = "Live Support"
    case cart = "Cart"
    case promotion = "Promotions"
    case notification = "Notifications"
    case compare = "Compare Products"
    case favorites = "My Favorites"
    case pending = "Pending Orders"
    case schedule = "Schedule Order"
    case rate = "Rate My Order"
    case history = "Order History"
    case points = "Loyality Points"
    case share = "Share App"
    case condition = "Terms & Conditions"
    case about = "About Us"
    case setting = "Settings"
    var idname :String{
        return self.rawValue
    }
}
enum ViewNibName : String{
    case nibOne = "ViewSectionHeader"
    var idview : String{
        return self.rawValue
    }
}
enum JsonData : String{
    case areaId = "areaId"
    case areaIdString = "201"
    case countryId = "countryId"
    case countryIdString = "1"
    var idJson : String{
        return self.rawValue
    }
}
enum ViewString : String{
    case offer = "Offer"
    case recommonded = "Recommended"
    case account = "My Account"
    case ok = "OK"
    case alert = "ALERT"
    case print = "error occured"
    var idOfferRec : String{
        return self.rawValue
    }
}
enum Font : String{
    case font = "Avenir next"
    var idFont : String{
        return self.rawValue
    }
}
enum SlideOutMenu : String{
    case sliderbar = "IdentifierTableCell"
    var idSlider : String{
        return self.rawValue
    }
}
enum Unwrapping : String{
    case nilWrap = "NIL"
    case error  = "error"
    var idNil : String{
        return self.rawValue
    }
}
enum Url : String{
    case urlAddress = "https://appbean.clikat.com/get_all_category"
    var idUrl : String{
        return self.rawValue
    }
}
enum StoryBoard : String{
    case identifierStory = "IdentifierStory"
    var idStory : String{
        return self.rawValue
    }
}




