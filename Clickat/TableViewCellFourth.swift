//
//  TableViewCellFourth.swift
//  Clickat
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class TableViewCellFourth: UITableViewCell {
    @IBOutlet var collectionRecomOutlet: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = UINib(nibName: "CollectionViewCellFourth", bundle: nil)
        collectionRecomOutlet.register(nib, forCellWithReuseIdentifier: "IdentifierRecommended" )
        collectionRecomOutlet.dataSource = self
        collectionRecomOutlet.delegate = self
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
extension TableViewCellFourth : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IdentifierRecommended", for: indexPath)
        cell.layer.cornerRadius = 1
        return cell
    }
}
