//
//  Alert.swift
//  Clickat
//
//  Created by Sierra 4 on 15/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import UIKit
func alertbox(_Message:String,obj:UIViewController)
{
    let alertmsg=UIAlertController(title: ViewString.alert.idOfferRec, message: _Message, preferredStyle: UIAlertControllerStyle.alert)
    let OkAction=UIAlertAction(title: ViewString.ok.idOfferRec, style: UIAlertActionStyle.default, handler: nil)
    alertmsg.addAction(OkAction)
    obj.present(alertmsg,animated:true,completion: nil)
    
}

