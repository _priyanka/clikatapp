//
//  SlideViewController.swift
//  Clickat
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class SlideViewController: UIViewController {

    @IBOutlet var footerView: UIView!
    @IBOutlet var tableOutlet: UITableView!
    @IBOutlet var viewOutlet: UIView!
    @IBOutlet var imgOutlet: UIImageView!
    var name : String!
    var headerView : ViewSectionHeader?
    var arrayOfSections = [
        [TableClass (image:#imageLiteral(resourceName: "ic_home_white"),name: CellNames.home.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_live_help_white"),name:CellNames.live.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_shopping_cart_white"),name:CellNames.cart.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_bookmark_border_white"),name:CellNames.promotion.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_notifications_none_white"),name:CellNames.notification.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_compare_white"),name:CellNames.compare.idname)],
        [TableClass (image:#imageLiteral(resourceName: "ic_favorite_border_white"),name:CellNames.favorites.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_filter_none_white"),name:CellNames.pending.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_class_white"),name:CellNames.schedule.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_star_rate_white_18pt"),name:CellNames.rate.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_indeterminate_check_box_white"),name:CellNames.history.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_loyalty_white"),name:CellNames.points.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_share_white-1"),name:CellNames.share.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_assignment_white"),name:CellNames.condition.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_account_circle_white"),name:CellNames.about.idname),
         TableClass (image:#imageLiteral(resourceName: "ic_settings_applications_white"),name:CellNames.setting.idname)]
    ]
       
    override func viewDidLoad() {
        super.viewDidLoad()
        let swipeLeft = UISwipeGestureRecognizer(target: self, action:#selector(hole))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        imgOutlet.layer.cornerRadius = 25
        self.imgOutlet.layer.borderWidth = 2
        self.imgOutlet.layer.borderColor = UIColor(red:1, green:1, blue:1, alpha: 1.0).cgColor
        
        self.viewOutlet.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.viewOutlet.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.sendSubview(toBack: blurEffectView)
        
        self.tableOutlet.backgroundColor = UIColor.clear
        let blur = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurView = UIVisualEffectView(effect: blur)
        blurView.frame = self.view.frame
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.tableOutlet.backgroundView = blurView
        view.addSubview(blurView)
        view.sendSubview(toBack: blurView)
        }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    Disappear.backSlideLeft(obj : self)
    }
func hole(gesture: UISwipeGestureRecognizer)
{
    let swipeGesture = gesture as UISwipeGestureRecognizer
        if (swipeGesture.direction == UISwipeGestureRecognizerDirection.left)
        {
            Disappear.backSlideLeft(obj : self)
        }
    }
}
extension SlideViewController: UITableViewDataSource ,UITableViewDelegate  {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = UINib(nibName: ViewNibName.nibOne.idview, bundle: nil)
        let headerView = nib.instantiate(withOwner: nil, options: nil)[0] as? ViewSectionHeader
        headerView?.viewAllOutlet.textColor = UIColor.white
        headerView?.uiviewOutlet.backgroundColor = UIColor(red: 123/255, green: 123/255, blue: 124/255, alpha: 1)
        headerView?.viewAllOutlet.font = UIFont(name: (headerView?.viewAllOutlet.font.fontName)!, size: 12.0)
        headerView?.viewAllOutlet.font = UIFont.boldSystemFont(ofSize: 12.0)
        headerView?.viewAllOutlet.text = ViewString.account.idOfferRec
        headerView?.buttonOutlet.isHidden = true
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0)
        {
            return 0
        }else{
            return 35
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayOfSections[section].count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayOfSections.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:SliderMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier:SlideOutMenu.sliderbar.idSlider, for: indexPath) as!SliderMenuTableViewCell
        cell.object = arrayOfSections[indexPath.section][indexPath.row]
        return cell
    }
}





