//
//  ObjectClass.swift
//  Clickat
//
//  Created by Sierra 4 on 10/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import UIKit
class  TableClass {
    var name: String?
    var image : UIImage?
    init(image : UIImage , name : String?) {
        self.name = name
        self.image = image
    }
}
